package protoyaml

import (
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoreflect"
	descriptorpb "google.golang.org/protobuf/types/descriptorpb"
	"gopkg.in/yaml.v3"
)

const (
	DelegateMatchUnspecified DelegateMatch = DelegateMatch_DELEGATE_MATCH_UNSPECIFIED
	DelegateMatchAllFields                 = DelegateMatch_DELEGATE_MATCH_ALL_FIELDS
	DelegateMatchFirstKey                  = DelegateMatch_DELEGATE_MATCH_FIRST_KEY
)

type messageDelegateMatcher func(*Node, protoreflect.FieldDescriptors) protoreflect.FieldDescriptor

var (
	messageDelegateMatchers = map[DelegateMatch]messageDelegateMatcher{
		DelegateMatchUnspecified: matchDelegateAllFields,
		DelegateMatchAllFields:   matchDelegateAllFields,
		DelegateMatchFirstKey:    matchDelegateFirstKey,
	}
)

// MessageDelegate represents information about a message delegate, namely the
// message field that should be populated by the YAML node.
type MessageDelegate interface {
	// Field returns the descriptor for the field that should act as the message
	// delegate and by populated by the YAML node.
	Field() protoreflect.FieldDescriptor

	// MatchedBy returns the method by which a oneof field specified as the
	// message delegate was matched.
	MatchedBy() DelegateMatch
}

type messageDelegate struct {
	field     protoreflect.FieldDescriptor
	matchedBy DelegateMatch
}

func (md *messageDelegate) Field() protoreflect.FieldDescriptor {
	return md.field
}

func (md *messageDelegate) MatchedBy() DelegateMatch {
	return md.matchedBy
}

// MessageDelegate returns the field that has been assigned as the message
// delegate using the `protoyaml.delegate` message option.
//
// # Example proto
//
//	message Option {
//	  option (protoyaml.delegate) = "opt";
//
//	  oneof opt {
//	    option (protoyaml.match) = MATCH_FIRST_KEY;
//
//	    Host host = 1;
//	    Cache cache = 2;
//	  }
//	}
func (node *Node) MessageDelegate(message protoreflect.Message) MessageDelegate {
	md := message.Descriptor()
	options := md.Options()

	if options == nil {
		return nil
	}

	name := proto.GetExtension(options.(*descriptorpb.MessageOptions), E_Delegate).(string)

	if name == "" {
		return nil
	}

	delegateName := protoreflect.Name(name)

	oneof := md.Oneofs().ByName(delegateName)
	if oneof != nil {
		matchBy := DelegateMatchAllFields
		options := oneof.Options()

		if options != nil {
			matchBy = proto.GetExtension(options.(*descriptorpb.OneofOptions), E_DelegateMatch).(DelegateMatch)
		}

		matcher, ok := messageDelegateMatchers[matchBy]
		if ok {
			field := matcher(node, oneof.Fields())

			if field != nil {
				return &messageDelegate{
					field:     field,
					matchedBy: matchBy,
				}
			}
		}
	}

	field := md.Fields().ByName(delegateName)
	if field != nil {
		return &messageDelegate{
			field:     field,
			matchedBy: DelegateMatchUnspecified,
		}
	}

	return nil
}

func matchDelegateFirstKey(node *Node, fields protoreflect.FieldDescriptors) protoreflect.FieldDescriptor {
	if node.Kind == yaml.MappingNode && len(node.Content) > 0 {
		field := fields.ByName(protoreflect.Name(node.Content[0].Value))
		if field == nil {
			return nil
		}

		switch field.Kind() {
		case protoreflect.MessageKind, protoreflect.GroupKind:
			return field
		}
	}

	return nil
}

func matchDelegateAllFields(node *Node, fields protoreflect.FieldDescriptors) protoreflect.FieldDescriptor {
	for i := 0; i < fields.Len(); i++ {
		field := fields.Get(i)
		match := false

		switch field.Kind() {
		case protoreflect.MessageKind, protoreflect.GroupKind:
			md := field.Message()
			fds := md.Fields()
			match = true

			// Does the message have fields for all the mapping node keys?
			for _, key := range node.Keys() {
				if fds.ByName(protoreflect.Name(key.Value)) == nil {
					match = false
					break
				}
			}
		}

		if match {
			return field
		}
	}

	return nil
}
