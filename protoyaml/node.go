package protoyaml

import (
	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

// Unmarshaler describes an interface that can decode a [Node]
type Unmarshaler interface {
	UnmarshalProtoYAML(*Node) error
}

// Node encapsulates a [yaml.Node] and provides methods for parsing YAML nodes
// into protobuf types
type Node struct {
	*yaml.Node
}

// Errorf returns an [error] with line/column information from the
// encapsulated [yaml.Node]
func (node *Node) Errorf(msg string, v ...any) error {
	return errors.Errorf(
		"(line %d col %d): "+msg,
		append([]any{node.Line, node.Column}, v...)...,
	)
}

// Entry returns a single entry of a YAML mapping node
func (node *Node) Entry(key string) (*Node, error) {
	if node.Kind != yaml.MappingNode || len(node.Content) < 2 {
		return nil, node.Errorf("expecting a map, got %q", node.ShortTag())
	}

	for i := 1; i < len(node.Content); i += 2 {
		if node.Content[i-1].Value == key {
			return &Node{node.Content[i]}, nil
		}
	}

	return nil, node.Errorf("entry %q not found in map", key)
}

// WithEntry calls the given function for a single entry in the YAML mapping
// node
func (node *Node) WithEntry(key string, fn func(*Node) error) error {
	entry, err := node.Entry(key)

	if err != nil {
		return err
	}

	return fn(entry)
}

// Len returns the _effective_ content length. For YAML mapping nodes, this is
// the number of entries. For YAML sequence nodes (and all other node types),
// this is the same as `len(node.Content)`.
func (node *Node) Len() int {
	if node.Kind == yaml.MappingNode {
		return len(node.Content) / 2
	}

	return len(node.Content)
}

// WithPairs calls the function on each key/value pair in the YAML mapping
// node. It returns upon the first error returned by the function.
func (node *Node) WithPairs(fn func(i int, k, v *Node) error) error {
	for i := 1; i < len(node.Content); i += 2 {
		key := &Node{node.Content[i-1]}
		value := &Node{node.Content[i]}

		err := fn(i/2, key, value)

		if err != nil {
			return err
		}
	}

	return nil
}

// Keys returns all key nodes of the mapping YAML node.
func (node *Node) Keys() []*Node {
	if node.Kind != yaml.MappingNode || len(node.Content) < 2 {
		return []*Node{}
	}

	keys := make([]*Node, len(node.Content)/2)

	for i := 0; i < len(node.Content); i += 2 {
		keys[i/2] = &Node{node.Content[i]}
	}

	return keys
}
