package protoyaml

import (
	"testing"

	"github.com/stretchr/testify/require"
	"google.golang.org/protobuf/reflect/protoreflect"
)

func TestUnmarshal(t *testing.T) {
	req := require.New(t)

	op := &Op{}

	req.NoError(Unmarshal(
		byteLines(
			"run: make",
		),
		op,
	))

	run := op.GetRun()
	req.NotNil(run)
	req.Equal("make", run.Command)
}

func TestDecodeProto(t *testing.T) {
	req := require.New(t)

	node := newNode(t, "run: make")
	op := &Op{}

	node.DecodeProto(op)

	run := op.GetRun()
	req.NotNil(run)
	req.Equal("make", run.Command)
}

func TestDecodeNew(t *testing.T) {
	req := require.New(t)

	value, err := DecodeNew[string](newNode(t, "foo"))

	req.NoError(err)
	req.True(protoreflect.ValueOfString("foo").Equal(value))
}

func TestDecodeScalar(t *testing.T) {
	testCases := []struct {
		name     string
		kind     protoreflect.Kind
		yaml     string
		expected protoreflect.Value
	}{
		{"bool", protoreflect.BoolKind, "true", protoreflect.ValueOf(true)},
		{"int32", protoreflect.Int32Kind, "123", protoreflect.ValueOf(int32(123))},
		{"sint32", protoreflect.Sint32Kind, "123", protoreflect.ValueOf(int32(123))},
		{"sfixed32", protoreflect.Sfixed32Kind, "123", protoreflect.ValueOf(int32(123))},
		{"int64", protoreflect.Int64Kind, "123", protoreflect.ValueOf(int64(123))},
		{"sint64", protoreflect.Sint64Kind, "123", protoreflect.ValueOf(int64(123))},
		{"sfixed64", protoreflect.Sfixed64Kind, "123", protoreflect.ValueOf(int64(123))},
		{"uint32", protoreflect.Uint32Kind, "123", protoreflect.ValueOf(uint32(123))},
		{"fixed32", protoreflect.Fixed32Kind, "123", protoreflect.ValueOf(uint32(123))},
		{"uint64", protoreflect.Uint64Kind, "123", protoreflect.ValueOf(uint64(123))},
		{"fixed64", protoreflect.Fixed64Kind, "123", protoreflect.ValueOf(uint64(123))},
		{"float", protoreflect.FloatKind, "12.3", protoreflect.ValueOf(float32(12.3))},
		{"double", protoreflect.DoubleKind, "12.3", protoreflect.ValueOf(float64(12.3))},
		{"string", protoreflect.StringKind, "foo", protoreflect.ValueOf("foo")},
		{"string/single quoted", protoreflect.StringKind, "'foo'", protoreflect.ValueOf("foo")},
		{"string/double quoted", protoreflect.StringKind, `"foo"`, protoreflect.ValueOf("foo")},
		{"string/block folded", protoreflect.StringKind, ">-\n foo", protoreflect.ValueOf("foo")},
		{"string/block unfolded", protoreflect.StringKind, "|-\n foo", protoreflect.ValueOf("foo")},
		{"base64/padding", protoreflect.BytesKind, "8J+Mvw==", protoreflect.ValueOf([]byte("🌿"))},
		{"base64/no padding", protoreflect.BytesKind, "8J+Mvw", protoreflect.ValueOf([]byte("🌿"))},
		{"base64url/padding", protoreflect.BytesKind, "8J-Mvw==", protoreflect.ValueOf([]byte("🌿"))},
		{"base64/no padding", protoreflect.BytesKind, "8J-Mvw", protoreflect.ValueOf([]byte("🌿"))},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			req := require.New(t)
			node := newNode(t, testCase.yaml)

			value, err := node.DecodeScalar(testCase.kind)

			req.NoError(err)
			req.True(value.Equal(testCase.expected))
		})
	}
}

func TestDecodeEnum(t *testing.T) {
	t.Run("valid", func(t *testing.T) {
		req := require.New(t)

		cache := &Cache{}
		field := cache.ProtoReflect().Descriptor().Fields().ByName("access")

		actual, err := newNode(t, "locked").DecodeEnum(field)
		expected := protoreflect.ValueOf(protoreflect.EnumNumber(CacheAccess_value["CACHE_ACCESS_LOCKED"]))

		req.NoError(err)
		req.True(actual.Equal(expected))
	})

	t.Run("invalid", func(t *testing.T) {
		req := require.New(t)

		cache := &Cache{}
		field := cache.ProtoReflect().Descriptor().Fields().ByName("access")

		_, err := newNode(t, "bogus").DecodeEnum(field)

		req.Error(err)
	})
}

func TestDecodeField(t *testing.T) {
	t.Run("message", func(t *testing.T) {
		t.Run("Unmarshaler", func(t *testing.T) {
			req := require.New(t)

			cp := &Copy{}
			message := cp.ProtoReflect()
			field := message.Descriptor().Fields().ByName("from")
			node := newNode(t, "foo-fs-ref")

			err := node.DecodeField(message, field)

			req.NoError(err)

			req.Equal("foo-fs-ref-set-by-UnmarshalProtoYAML", cp.From.Ref)
		})

		t.Run("yaml.Unmarshaler", func(t *testing.T) {
			req := require.New(t)

			cp := &Copy{}
			message := cp.ProtoReflect()
			field := message.Descriptor().Fields().ByName("destination")
			node := newNode(t, "/foo/path")

			err := node.DecodeField(message, field)

			req.NoError(err)

			req.Equal("/foo/path-set-by-UnmarshalYAML", cp.Destination.Path)
		})

		t.Run("ProtoReflect", func(t *testing.T) {
			req := require.New(t)

			cp := &Copy{}
			message := cp.ProtoReflect()
			field := message.Descriptor().Fields().ByName("ctime")
			node := newNode(t, "123")

			err := node.DecodeField(message, field)

			req.NoError(err)

			req.Equal(int64(123), cp.Ctime.Value)
		})

		t.Run("Enum", func(t *testing.T) {
			req := require.New(t)

			cache := &Cache{}
			message := cache.ProtoReflect()
			field := message.Descriptor().Fields().ByName("access")
			node := newNode(t, "locked")

			err := node.DecodeField(message, field)

			req.NoError(err)

			req.Equal(CacheAccess_CACHE_ACCESS_LOCKED, cache.Access)
		})

		t.Run("Scalar", func(t *testing.T) {
			req := require.New(t)

			cache := &Cache{}
			message := cache.ProtoReflect()
			field := message.Descriptor().Fields().ByName("target")
			node := newNode(t, "/foo/path")

			err := node.DecodeField(message, field)

			req.NoError(err)

			req.Equal("/foo/path", cache.Target)
		})
	})
}

func TestDecodeMessage(t *testing.T) {
	t.Run("no fields", func(t *testing.T) {
		req := require.New(t)

		nofields := &NoFields{}
		node := newNode(t, "foo")

		err := node.DecodeMessage(nofields.ProtoReflect())

		req.NoError(err)
	})

	t.Run("wrapper (single value field)", func(t *testing.T) {
		req := require.New(t)

		ts := &Timestamp{}
		node := newNode(t, "123")

		err := node.DecodeMessage(ts.ProtoReflect())

		req.NoError(err)
		req.Equal(ts.Value, int64(123))
	})

	t.Run("message from map", func(t *testing.T) {
		req := require.New(t)

		run := &Run{}
		node := newNode(
			t,
			"command: make",
			"arguments: [foo, bar]",
			"options:",
			"  - host: foo.test",
			"    ip: 1.2.3.4",
			"  - cache: /foo/cache/path",
			"    access: locked",
			"  - long-name: foo",
		)

		err := node.DecodeMessage(run.ProtoReflect())

		req.NoError(err)
		req.Equal("make", run.Command)
		req.Equal([]string{"foo", "bar"}, run.Arguments)

		req.Len(run.Options, 3)

		host := run.Options[0].GetHost()
		req.NotNil(host)
		req.Equal("foo.test", host.Name)
		req.Equal("1.2.3.4", host.Ip)

		cache := run.Options[1].GetCache()
		req.NotNil(cache)
		req.Equal("/foo/cache/path", cache.Target)
		req.Equal(CacheAccess_CACHE_ACCESS_LOCKED, cache.Access)

		longName := run.Options[2].GetLongName()
		req.Equal("foo", longName)
	})
}

func TestDecodeList(t *testing.T) {
	t.Run("scalars", func(t *testing.T) {
		req := require.New(t)

		phrase := &Phrase{}
		node := newNode(t, "[a, stitch, in, time]")

		message := phrase.ProtoReflect()
		field := message.Descriptor().Fields().ByName("words")
		list := message.Mutable(field).List()

		err := node.DecodeList(field, list)
		req.NoError(err)

		req.Equal([]string{"a", "stitch", "in", "time"}, phrase.Words)
	})
	t.Run("enums", func(t *testing.T) {
		req := require.New(t)

		bleeps := &Bleeps{}
		node := newNode(t, "[1, 2, 0, 2, 1]")

		message := bleeps.ProtoReflect()
		field := message.Descriptor().Fields().ByName("blerps")
		list := message.Mutable(field).List()

		err := node.DecodeList(field, list)
		req.NoError(err)

		req.Equal([]Blerp{1, 2, 0, 2, 1}, bleeps.Blerps)
	})
}

func TestDecodeMap(t *testing.T) {
	req := require.New(t)

	pairs := &Pairs{}
	node := newNode(t, "{ noemie: reeky, gigi: bunbun }")

	message := pairs.ProtoReflect()
	field := message.Descriptor().Fields().ByName("pairs")
	mmap := message.Mutable(field).Map()

	err := node.DecodeMap(field, mmap)
	req.NoError(err)

	req.Equal(map[string]string{"noemie": "reeky", "gigi": "bunbun"}, pairs.Pairs)
}
