package protoyaml

import (
	"encoding/base64"
	"fmt"
	"strings"

	"github.com/iancoleman/strcase"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoreflect"
	"gopkg.in/yaml.v3"
)

const (
	// WrapperFieldName is the name of the single field (`value`) used by
	// common wrapper types
	//
	// See https://protobuf.dev/reference/protobuf/google.protobuf/
	WrapperFieldName protoreflect.Name = protoreflect.Name("value")
)

// DecodeProto decodes the current YAML node into the given [proto.Message]
// implementation. See [DecodeMessage] for parsing rules.
func (node *Node) DecodeProto(pm proto.Message) error {
	proto.Reset(pm)

	return node.DecodeMessage(pm.ProtoReflect())
}

// decodeProtoTryUnmarshalers decodes the current YAML node into the given
// [proto.Message].
//
// # Rules
//
// ## Interface implements [Unmarshaler]
//
//  1. Call UnmarshalProtoYAML on interface
//
// ## Interface implements [yaml.Unmarshaler]
//
//  1. Call UnmarshalYAML on interface
//
// ## Otherwise
//
//  1. Decode with [DecodeMessage]
func (node *Node) decodeProtoTryUnmarshalers(pm proto.Message) error {
	proto.Reset(pm)

	protoUn, ok := pm.(Unmarshaler)
	if ok {
		return protoUn.UnmarshalProtoYAML(node)
	}

	un, ok := pm.(yaml.Unmarshaler)
	if ok {
		return un.UnmarshalYAML(node.Node)
	}

	return node.DecodeMessage(pm.ProtoReflect())
}

// DecodeMessage decodes the current YAML node into the given
// [protoreflect.Message].
//
// #Rules
//
// ## Message contains no fields
//
//  1. Discard node
//
// ## Message contains a `protoyaml.delegate` option
//
//  1. Resolve delegate
//  2. Decode to delegate field using [DecodeField]
//
// ## Message contains single field called `value`
//
//  1. Assume a wrapper message type
//  2. Decode this node to single field
//
// ## Mapping
//
//  1. Iterate over node pairs.
//  2. Replace `-` in key name with `_`. Note case is not converted.
//  3. Match key to message fields by name.
//  4. Check oneof encapsulated fields first, disregarding oneof name.
//  5. Check other fields next.
//  6. Upon a match, decode pair value node to field. See [DecodeField] for
//     field rules.
//
// ## Other
//
//  1. Discard node
func (node *Node) DecodeMessage(message protoreflect.Message) error {
	md := message.Descriptor()
	fields := md.Fields()
	numFields := fields.Len()

	if numFields < 1 {
		return nil
	}

	delegate := node.MessageDelegate(message)

	if delegate != nil {
		if delegate.MatchedBy() == DelegateMatchFirstKey {
			// rename the first mapping key to match the first field in the delegate
			// TODO This feels really gross. Do something better
			dfields := delegate.Field().Message().Fields()
			if dfields.Len() > 0 {
				node.Content[0].Value = string(dfields.Get(0).Name())
			}
		}

		return node.DecodeField(message, delegate.Field())
	}

	switch fields.Len() {
	case 0:
		return nil
	case 1:
		field := fields.ByName(WrapperFieldName)
		if field != nil {
			return node.DecodeField(message, field)
		}
	}

	if node.Kind != yaml.MappingNode {
		return nil
	}

	oneofs := md.Oneofs()
	numOneofs := oneofs.Len()

	return node.WithPairs(func(_ int, key, value *Node) error {
		fieldName := protoreflect.Name(strings.ReplaceAll(key.Value, "-", "_"))

		for i := 0; i < numOneofs; i++ {
			field := oneofs.Get(i).Fields().ByName(fieldName)
			if field != nil {
				return value.DecodeField(message, field)
			}
		}

		field := fields.ByName(fieldName)
		if field != nil {
			return value.DecodeField(message, field)
		}

		return nil
	})
}

// DecodeField decodes the YAML node into the given field.
//
// # Rules
//
// ## Field is a map
//
//  1. Decode field using [DecodeMap]
//
// ## Field is a list
//
//  1. Decode field using [DecodeList]
//
// ## Field is a message
//
//  1. If underlying interface implements [Unmarshaler], call
//     UnmarshalProtoYAML on interface.
//  2. If underlying interface implements [yaml.Unmarshaler], call
//     UnmarshalYAML on interface.
//  2. Otherwise, recurse on field's [protoreflect.Message] with
//     [DecodeMessage].
//
// ## Field is an enum
//
//  1. Decode field using [DecodeEnum].
//
// ## Otherwise
//
//  1. Decode field using [DecodeScalar].
func (node *Node) DecodeField(message protoreflect.Message, field protoreflect.FieldDescriptor) error {
	if field.Cardinality() == protoreflect.Repeated {
		if field.IsMap() {
			return node.DecodeMap(field, message.Mutable(field).Map())
		}

		return node.DecodeList(field, message.Mutable(field).List())
	}

	kind := field.Kind()

	var value protoreflect.Value
	var err error

	switch kind {
	case protoreflect.MessageKind, protoreflect.GroupKind:
		value = message.NewField(field)
		err = node.decodeProtoTryUnmarshalers(value.Message().Interface())
	case protoreflect.EnumKind:
		value, err = node.DecodeEnum(field)
	default:
		value, err = node.DecodeScalar(kind)
	}

	if err != nil {
		return err
	}

	if value.IsValid() {
		message.Set(field, value)
	}

	return nil
}

// DecodeMap decodes the YAML node to a mutable repeated (map) value.
//
// # Rules
//
// ## Node is a map
//
//  1. Decode each content node pair into a key/value and append the map.
//
// ## Otherwise, error
func (node *Node) DecodeMap(field protoreflect.FieldDescriptor, mmap protoreflect.Map) error {
	if node.Kind != yaml.MappingNode {
		return node.Errorf("cannot decode a %s to a map", node.ShortTag())
	}

	keyfield := field.MapKey()
	keykind := keyfield.Kind()
	valuefield := field.MapValue()
	valuekind := valuefield.Kind()

	return node.WithPairs(func(_ int, keyNode *Node, valueNode *Node) error {
		key, err := keyNode.DecodeScalar(keykind)

		if err != nil {
			return err
		}

		var value protoreflect.Value

		switch valuekind {
		case protoreflect.MessageKind, protoreflect.GroupKind:
			value = mmap.NewValue()
			err = valueNode.DecodeMessage(value.Message())
		default:
			value, err = valueNode.DecodeScalar(valuekind)
		}

		if err != nil {
			return err
		}

		if value.IsValid() {
			mmap.Set(key.MapKey(), value)
		}

		return nil
	})
}

// DecodeList decodes the YAML node to a mutable repeated (list) value.
//
// # Rules
//
// ## Node is a sequence
//
//  1. Decode each content node to a new list value and append.
//
// ## Otherwise, error
func (node *Node) DecodeList(field protoreflect.FieldDescriptor, list protoreflect.List) error {
	if node.Kind != yaml.SequenceNode {
		return node.Errorf("cannot decode a %s to a list", node.ShortTag())
	}

	kind := field.Kind()

	for _, n := range node.Content {
		node := &Node{n}

		var value protoreflect.Value
		var err error

		switch kind {
		case protoreflect.MessageKind, protoreflect.GroupKind:
			value = list.NewElement()
			err = node.DecodeMessage(value.Message())
		case protoreflect.EnumKind:
			value, err = node.DecodeEnum(field)
		default:
			value, err = node.DecodeScalar(kind)
		}

		if err != nil {
			return err
		}

		if value.IsValid() {
			list.Append(value)
		}
	}

	return nil
}

// DecodeEnum decodes the YAML node to an enum field.
//
// # Rules
//
// ## Node is a !!str
//
//  1. Convert the node value to uppercase
//  2. Resolve an enum value by name
//  3. If not found, try to prefix the node value with upper underscore case
//     version of enum name (e.g. `CACHE_ACCESS_` for `CacheAccess`)
//  4. Resolve an enum value by name
//
// ## Node is an !!int
//
//  1. Resolve an enum value by number
//
// ## Otherwise
//
//  1. Error
func (node *Node) DecodeEnum(field protoreflect.FieldDescriptor) (protoreflect.Value, error) {
	kind := field.Kind()
	enum := field.Enum()
	tag := node.ShortTag()

	if kind != protoreflect.EnumKind || enum == nil {
		return protoreflect.Value{}, node.Errorf("field is not an enum, it is a %s", kind)
	}

	var enumValue protoreflect.EnumValueDescriptor

	switch tag {
	case "!!str":
		nodeValue := strings.ToUpper(node.Value)
		enumValue = enum.Values().ByName(protoreflect.Name(nodeValue))

		// Values are allowed to omit the enum prefix if the prefix is based on
		// the enum name. Convert the enum name to a prefix and try again.
		if enumValue == nil {
			enumValue = enum.Values().ByName(protoreflect.Name(
				fmt.Sprintf(
					"%s_%s",
					strcase.ToScreamingSnake(string(enum.Name())),
					nodeValue,
				),
			))
		}
	case "!!int":
		var num int32
		err := node.Decode(&num)
		if err != nil {
			return protoreflect.Value{}, err
		}

		enumValue = enum.Values().ByNumber(protoreflect.EnumNumber(num))
	default:
		return protoreflect.Value{}, node.Errorf("enum values must either be a !!str or !!int, not %s", tag)
	}

	if enumValue != nil {
		return protoreflect.ValueOfEnum(enumValue.Number()), nil
	}

	return protoreflect.Value{}, node.Errorf("invalid enum value %q", node.Value)
}

// DecodeScalar decodes the YAML node to a new scalar [protoreflect.Value].
func (node *Node) DecodeScalar(kind protoreflect.Kind) (protoreflect.Value, error) {
	switch kind {
	case protoreflect.BoolKind:
		return DecodeNew[bool](node)

	case protoreflect.Int32Kind, protoreflect.Sint32Kind, protoreflect.Sfixed32Kind:
		return DecodeNew[int32](node)

	case protoreflect.Int64Kind, protoreflect.Sint64Kind, protoreflect.Sfixed64Kind:
		return DecodeNew[int64](node)

	case protoreflect.Uint32Kind, protoreflect.Fixed32Kind:
		return DecodeNew[uint32](node)

	case protoreflect.Uint64Kind, protoreflect.Fixed64Kind:
		return DecodeNew[uint64](node)

	case protoreflect.FloatKind:
		return DecodeNew[float32](node)

	case protoreflect.DoubleKind:
		return DecodeNew[float64](node)

	case protoreflect.StringKind:
		return DecodeNew[string](node)

	case protoreflect.BytesKind:
		return node.DecodeBytes()
	}

	return protoreflect.Value{}, node.Errorf("field is not a valid scalar kind, it is a %s", kind)
}

// DecodeBytes decodes the YAML node with base64 encoded data and decodes it
// into a new [protoreflect.Value].
func (node *Node) DecodeBytes() (protoreflect.Value, error) {
	var str string

	err := node.Decode(&str)
	if err != nil {
		return protoreflect.Value{}, err
	}

	// see https://github.com/protocolbuffers/protobuf-go/blob/v1.31.0/encoding/protojson/decode.go#L463
	enc := base64.StdEncoding
	if strings.ContainsAny(str, "-_") {
		enc = base64.URLEncoding
	}
	if len(str)%4 != 0 {
		enc = enc.WithPadding(base64.NoPadding)
	}

	bytes, err := enc.DecodeString(str)
	if err != nil {
		return protoreflect.Value{}, err
	}

	return protoreflect.ValueOfBytes(bytes), nil
}

// DecodeNew[T] takes a YAML node and returns a new [protoreflect.Value] based
// on a new T
func DecodeNew[T any](node *Node) (protoreflect.Value, error) {
	v := new(T)
	err := node.Decode(v)
	return protoreflect.ValueOf(*v), err
}
