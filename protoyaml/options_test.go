package protoyaml

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMessageDelegate(t *testing.T) {
	t.Run("Match", func(t *testing.T) {
		t.Run("ALL_FIELDS", func(t *testing.T) {
			t.Run("best field", func(t *testing.T) {
				req := require.New(t)
				node := newNode(
					t,
					"field1: foo",
					"field2: bar",
					"field3: baz",
				)

				foo := &Foo{}

				err := node.DecodeMessage(foo.ProtoReflect())
				req.NoError(err)

				req.IsType(&Foo_Opt2{}, foo.Opt)
			})

			t.Run("first field", func(t *testing.T) {
				req := require.New(t)
				node := newNode(
					t,
					"field1: foo",
					"field2: bar",
				)

				foo := &Foo{}

				err := node.DecodeMessage(foo.ProtoReflect())
				req.NoError(err)

				req.IsType(&Foo_Opt1{}, foo.Opt)
			})
		})

		t.Run("FIRST_KEY", func(t *testing.T) {
			req := require.New(t)
			node := newNode(
				t,
				"cache: /var/foo",
			)

			ro := &Run_Option{}

			err := node.DecodeMessage(ro.ProtoReflect())
			req.NoError(err)

			req.IsType(&Run_Option_Cache{}, ro.Opt)
		})
	})
}
