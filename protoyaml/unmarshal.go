package protoyaml

import (
	"google.golang.org/protobuf/proto"
	"gopkg.in/yaml.v3"
)

// Unmarshal decodes the given YAML data to a [proto.Message].
func Unmarshal(data []byte, message proto.Message) error {
	return yaml.Unmarshal(data, &yamlTarget{message})
}

type yamlTarget struct {
	message proto.Message
}

func (yt *yamlTarget) UnmarshalYAML(node *yaml.Node) error {
	return (&Node{node}).decodeProtoTryUnmarshalers(yt.message)
}
