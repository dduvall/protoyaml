package protoyaml

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestKeys(t *testing.T) {
	req := require.New(t)

	node := newNode(t,
		"key1: foo",
		"key2:",
	)

	keys := node.Keys()

	req.Len(keys, 2)
	req.Equal("key1", keys[0].Value)
	req.Equal("key2", keys[1].Value)
}
