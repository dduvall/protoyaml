package protoyaml

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

type testProtoFoo struct {
	unmarshaled bool
}

func (foo *testProtoFoo) UnmarshalYAML(node *yaml.Node) {
	foo.unmarshaled = true
}

type testProtoBar struct {
	unmarshaled bool
}

func (bar *testProtoBar) UnmarshalYAML(node *yaml.Node) {
	bar.unmarshaled = true
}

func (bar *testProtoBar) UnmarshalProtoYAML(node *Node) {
	bar.unmarshaled = true
}

type nodeGrabber struct {
	*Node
}

func (ng *nodeGrabber) UnmarshalYAML(yn *yaml.Node) error {
	ng.Node = &Node{yn}
	return nil
}

func byteLines(lines ...string) []byte {
	return []byte(strings.Join(lines, "\n") + "\n")
}

func newNode(t *testing.T, lines ...string) *Node {
	t.Helper()

	ng := &nodeGrabber{}
	require.NoError(t, yaml.Unmarshal(byteLines(lines...), ng))

	return ng.Node
}

// UnmarshalYAML should be called since Destination doesn't implement
// UnmarshalProtoYAML
func (dest *Destination) UnmarshalYAML(node *yaml.Node) error {
	err := node.Decode(&dest.Path)

	dest.Path += "-set-by-UnmarshalYAML"

	return err
}

// UnmarshalYAML should not be called as UnmarshalProtoYAML is preferred
func (fs *Filesystem) UnmarshalYAML(node *yaml.Node) error {
	err := node.Decode(&fs.Ref)

	fs.Ref += "-set-by-UnmarshalYAML"

	return err
}

// UnmarshalProtoYAML should be preferred over UnmarshalYAML
func (fs *Filesystem) UnmarshalProtoYAML(node *Node) error {
	err := node.Decode(&fs.Ref)

	fs.Ref += "-set-by-UnmarshalProtoYAML"

	return err
}
