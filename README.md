# ProtoYAML

ProtoYAML unmarshals YAML documents to [protobuf][protobuf] Go types.

## Purpose

ProtoYAML is intended for use where an application needs to parse some _user
written_ configuration directly to [protoc][protobuf] generated types. It does
not focus on efficiency nor does it guarantee correctness when parsing JSON
that conforms to the [Proto3 JSON specification][proto3json]. For that, use
[protojson][protojson].

## Features

### Basic unmarshalling

Say you have a protobuf schema defined in your application—this one is from
[Python][phyton] albeit simplified—and you want to allow users to write
descriptions somewhere in their configuration that parse to the schema.

```protobuf
message Op {
  oneof op {
    Image image = 1;
    Run run = 2;
    Copy copy = 3;
  }
}

message Image {
  string ref = 1;
}

message Run {
  string command = 1;
}

message Copy {
  message Option {
    oneof opt {
      int32 uid = 1;
      uint32 mode = 2;
    }
  }

  string source = 1;
  Filesystem from = 2;
  string destination = 3;
  repeated Option options = 4;
}

message Filesystem {
  string ref = 1;
}
```

ProtoYAML can unmarshal the following YAML to the Go types that were generated
from your protobuf by `protoc`.

```yaml
- image:
    ref: debian:bookworm
- run:
    command: apt-get install -y golang
- copy:
    source: ./src
    from:
     ref: local
    options:
      - uid: 123
      - mode: 0o644
- run:
    command: make
```

Just use `protoyaml.Unmarshal`.

```go
package foo

import "gitlab.wikimedia.org/dduvall/protoyaml"

func ParseConfig(data []byte) (Config, error) {
  cfg := Config{}
  err := protoyaml.Unmarshal(data, &cfg)
  return cfg, err
}
```

### Using message delegates

The first example shows some basic unmarshalling, but what if you want to
allow users an even more concise configuration format without writing a bunch
of custom unmarshalling code.

You can avoid requiring users to provide all the encapsulation that would be
necessary in JSON encoded protobuf using two available options.

 - `protobuf.delegate`. This `message` option tells the decoder to unmarshal
    map entries to a field of the message instead of to the message itself.
    This can be used to allow more concise configuration of wrapper types or
    repeated `oneof`s that are necessarily encapsulated by a parent message
    (since a protobuf `oneof` cannot be directly `repeated`).
 - `protobuf.delegate_match`. When used in conjunction with
   `protobuf.delegate`, this `oneof` option tells the message delegate handler
   to resolve a `oneof` field based on specific logic:

   - `DELEGATE_MATCH_ALL_FIELDS` meaning all the fields from the source map
     must be present in the destination `oneof` field message type.
   - `DELEGATE_MATCH_FIRST_KEY` meaning the first map key must match the name
     of the `oneof` field.

Let's modify the former protobuf definitions using these options and see what
happens.

```protobuf
message Op {
  // Pass all Op map entries off to the "op" oneof
  option (protoyaml.delegate) = "op";

  oneof op {
    // Resolve a oneof by matching the first key of the delegated map to a
    // field name
    option (protoyaml.delegate_match) = DELEGATE_MATCH_FIRST_KEY;

    Image image = 1;
    Run run = 2;
    Copy copy = 3;
  }
}

message Image {
  string ref = 1;
}

message Run {
  string command = 1;
}

message Copy {
  message Option {
    // A oneof can't be `repeated` so folks have to use encapsulating
    // messages. We don't want users to have to know about this implementation
    // detail when writing config. Pass off the option YAML map to the
    // encapsulated oneof.
    option (protoyaml.delegate) = "opt";

    oneof opt {
      // Match by first map key here as well.
      option (protoyaml.delegate_match) = DELEGATE_MATCH_FIRST_KEY;

      int32 uid = 1;
      uint32 mode = 2;
    }
  }

  string source = 1;
  Filesystem from = 2;
  string destination = 3;
  repeated Option options = 4;
}

message Filesystem {
  // Filesystem is wrapper type. Delegate parsing to the single field.
  option (protoyaml.delegate) = "ref";

  string ref = 1;
}
```

Now the user written YAML can be as simple as this.

```yaml
- image: debian:bookworm
- run: apt-get install -y golang
- copy: ./src
  from: local
  options:
    - uid: 123
    - mode: 0o644
- run: make
```

## Limitations

 - Does not currently support [Any types][any].
 - It is currently experimental.

## License

ProtoYAML is licensed under the GNU General Public License 3.0 or later
(GPL-3.0+). See the LICENSE file for more details.

[protobuf]: https://github.com/protocolbuffers/protobuf
[proto3json]: https://protobuf.dev/programming-guides/proto3/#json
[protojson]: https://pkg.go.dev/google.golang.org/protobuf/encoding/protojson
[phyton]: https://gitlab.wikimedia.org/dduvall/phyton
[any]: https://protobuf.dev/programming-guides/proto3/#any
