PROTO := $(shell find . -type f -name '*.proto' -not -path "./vendor/*")
PBGO := $(patsubst %.proto,%.pb.go,$(PROTO))

.PHONY: proto

proto: $(PBGO)
%.pb.go: %.proto
	protoc -I. --proto_path=. --go_out=. --go_opt=paths=source_relative $<

clean:
	rm $(PBGO)

test: proto
	go test ./...
